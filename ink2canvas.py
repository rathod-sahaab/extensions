#!/usr/bin/env python
# coding=utf-8
# Copyright (C) 2011 Karlisson Bezerra <contact@hacktoon.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from lxml import etree

import inkex

import ink2canvas_lib.svg as svg
from ink2canvas_lib.canvas import Canvas

class Ink2Canvas(inkex.OutputExtension):
    def save(self, stream):
        svg_root = self.document.getroot()
        width = self.svg.unittouu(svg_root.get("width"))
        height = self.svg.unittouu(svg_root.get("height"))
        canvas = Canvas(self, width, height)
        self.walk_tree(svg_root, canvas)
        stream.write(canvas.output().encode('utf-8'))

    def get_tag_name(self, node):
        # remove namespace part from "{http://www.w3.org/2000/svg}elem"
        return node.tag.split("}")[1]

    def get_gradient_defs(self, elem):
        url_id = elem.get_gradient_href()
        # get the gradient element
        gradient = self.svg.getElementById(url_id)
        # get the color stops
        url_stops = gradient.get(inkex.addNS("href", "xlink"))
        gstops = self.svg.getElement("//svg:linearGradient[@id='%s']" % url_stops[1:])
        colors = []
        for stop in gstops:
            colors.append(stop.get("style"))
        if gradient.get("r"):
            return svg.RadialGradientDef(gradient, colors)
        else:
            return svg.LinearGradientDef(gradient, colors)

    def get_clip_defs(self, elem):
        if elem.has_clip():
            pass
        return

    def walk_tree(self, root, canvas):
        for node in root:
            if node.tag is etree.Comment:
                continue
            tag = self.get_tag_name(node)
            class_name = tag.capitalize()
            if not hasattr(svg, class_name):
                continue
            gradient = None
            clip = None
            # creates a instance of 'elem'
            # similar to 'elem = Rect(tag, node, ctx)'
            elem = getattr(svg, class_name)(tag, node, canvas)
            if elem.has_gradient():
                gradient = self.get_gradient_defs(elem)
            elem.start(gradient)
            elem.draw()
            self.walk_tree(node, canvas)
            elem.end()


if __name__ == "__main__":
    Ink2Canvas().run()
