#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2011 Felipe Correa da Silva Sanches <juca@members.fsf.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

import inkex
from inkex.elements import Guide

class SVGFont2Layers(inkex.EffectExtension):
    def __init__(self):
        super(SVGFont2Layers, self).__init__()
        self.count = 0
        self.arg_parser.add_argument("--limitglyphs", type=inkex.Boolean, default=True,\
             help="Load only the first 30 glyphs from the SVGFont (otherwise the loading "
                  "process may take a very long time)")

    def create_horiz_guideline(self, label, y):
        return Guide(0, y, (0,1), inkscape__label=label)

    def flip_cordinate_system(self, elem, emsize, baseline):
        """Scale and translate the element's path, returns the path object"""
        path = elem.path
        path.scale(1, -1, inplace=True)
        path.translate(0, int(emsize) - int(baseline), inplace=True)
        return path

    def effect(self):
        # TODO: detect files with multiple svg fonts declared.
        # Current code only reads the first svgfont instance
        font = self.svg.defs.findone('svg:font')
        if font is None:
            return inkex.errormsg("There are no svg fonts")
        #setwidth = font.get("horiz-adv-x")
        baseline = font.get("horiz-origin-y")
        if baseline is None:
            baseline = 0

        fontface = font.findone('svg:font-face')

        # TODO: where should we save the font family name?
        # fontfamily = fontface.get("font-family")
        emsize = fontface.get("units-per-em")

        # TODO: should we guarantee that <svg:font horiz-adv-x> equals <svg:font-face units-per-em> ?
        caps = fontface.get("cap-height")
        xheight = fontface.get("x-height")
        ascender = fontface.get("ascent")
        descender = fontface.get("descent")

        self.svg.set("width", emsize)
        self.create_horiz_guideline("baseline", int(baseline))
        self.create_horiz_guideline("ascender", int(baseline) + int(ascender))
        self.create_horiz_guideline("caps", int(baseline) + int(caps))
        self.create_horiz_guideline("xheight", int(baseline) + int(xheight))
        self.create_horiz_guideline("descender", int(baseline) - int(descender))

        # TODO: missing-glyph
        for x, glyph in enumerate(font.findall('svg:glyph')):
            unicode_char = glyph.get("unicode")
            if unicode_char is None:
                continue

            layer = self.svg.add(inkex.Group.create("GlyphLayer-" + unicode_char, True))
            # glyph layers (except the first one) are innitially hidden
            if x == 0:
                layer.set("style", "display:none")

            # TODO: interpret option 1

            ############################
            # Option 1:
            # Using clone (svg:use) as childnode of svg:glyph

            # use = self.get_or_create(glyph, inkex.addNS('use', 'svg'))
            # use.set(inkex.addNS('href', 'xlink'), "#"+group.get("id"))
            # TODO: This code creates <use> nodes but they do not render on svg fonts dialog. why?

            ############################
            # Option 2:
            # Using svg:paths as childnodes of svg:glyph
            for elem in glyph.findall('svg:path'):
                new_path = layer.add(inkex.PathElement())
                new_path.path = self.flip_cordinate_system(elem, emsize, baseline)

            ############################
            # Option 3:
            # Using curve description in d attribute of svg:glyph
            path = layer.add(inkex.PathElement())
            path.path = self.flip_cordinate_system(glyph, emsize, baseline)

            self.count += 1
            if self.options.limitglyphs and self.count >= 30:
                break


if __name__ == '__main__':
    SVGFont2Layers().run()
